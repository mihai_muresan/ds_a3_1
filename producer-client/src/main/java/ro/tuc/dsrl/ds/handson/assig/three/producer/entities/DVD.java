package ro.tuc.dsrl.ds.handson.assig.three.producer.entities;

import com.google.gson.Gson;

public class DVD {
    private String title;
    private int year;
    private double price;

    public DVD(String title, int year, double price) {
        this.title = title;
        this.year = year;
        this.price = price;
    }

    public String getTitle() {
        return title;
    }

    public int getYear() {
        return year;
    }

    public double getPrice() {
        return price;
    }

    @Override
    public String toString() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }
}

package ro.tuc.dsrl.ds.handson.assig.three.consumer.start;

import com.google.gson.Gson;
import ro.tuc.dsrl.ds.handson.assig.three.consumer.connection.QueueServerConnection;
import ro.tuc.dsrl.ds.handson.assig.three.producer.entities.DVD;

import java.io.IOException;
import java.io.PrintWriter;

public class FileCreatorStart {
    public static void main(String[] args) {
        QueueServerConnection queue = new QueueServerConnection("localhost", 8888);

        String message;

        while (true) {
            try {
                message = queue.readMessage();

                Gson gson = new Gson();
                DVD dvd = gson.fromJson(message, DVD.class);

                System.out.println("Creating file");

                PrintWriter dvdFile = new PrintWriter(dvd.getTitle() + ".txt");
                dvdFile.println(dvd.toString());

                dvdFile.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
